package de.moritz.api;

import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.ProtocolManager;
import de.moritz.api.api.BossbarAPI;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.HashMap;
import java.util.UUID;

/**
 * Created by Moritz on 02.03.2017.
 */

public class APIPlugin extends JavaPlugin {

    private static APIPlugin instance;
    private static ProtocolManager protocolManager;
    private static HashMap<UUID, BossbarAPI> bossbarHashMap;

    @Override
    public void onEnable( ) {
        APIPlugin.instance = this;
        APIPlugin.protocolManager = ProtocolLibrary.getProtocolManager();
        APIPlugin.bossbarHashMap = new HashMap<>();

        getServer().getMessenger().registerOutgoingPluginChannel( this, "BungeeCord" );
    }

    public static APIPlugin getInstance( ) {
        return instance;
    }

    public static ProtocolManager getProtocolManager( ) {
        return protocolManager;
    }

    public static HashMap<UUID, BossbarAPI> getBossbarHashMap( ) {
        return bossbarHashMap;
    }
}
