package de.moritz.api.api;

import de.moritz.api.APIPlugin;
import org.bukkit.entity.Player;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;

/**
 * Created by Moritz on 02.03.2017.
 */

public class ConnectAPI {

    public static void joinServer( Player player, String server ) {
        try {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            DataOutputStream dataOutputStream = new DataOutputStream( byteArrayOutputStream );
            dataOutputStream.writeUTF( "Connect" );
            dataOutputStream.writeUTF( server );
            player.sendPluginMessage( APIPlugin.getInstance(), "BungeeCord", byteArrayOutputStream.toByteArray() );
        } catch ( IOException e ) {
            e.printStackTrace();
        }
    }

}
