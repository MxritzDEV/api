package de.moritz.api.api;

import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.BookMeta;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;
import org.bukkit.inventory.meta.SkullMeta;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Moritz on 02.03.2017.
 */

public class Item {

    private ItemStack itemStack;
    private ItemMeta itemMeta;
    private List<String> lore;
    private byte dyecolor;
    private byte potionid;
    private byte dataid;
    private LeatherArmorMeta armorMeta;
    private BookMeta bookMeta;
    private SkullMeta skullMeta;

    public Item( Material material, int amount ) {
        this.itemStack = new ItemStack( material, amount );
        this.itemMeta = this.itemStack.getItemMeta();
        this.lore = new ArrayList<>();
        this.dyecolor = 20;
        this.potionid = 0;
        this.dataid = 0;
        if ( this.isLeather() ) {
            this.armorMeta = (LeatherArmorMeta) this.itemStack.getItemMeta();
            return;
        }
        if ( material == Material.SKULL_ITEM ) {
            this.skullMeta = (SkullMeta) this.itemStack.getItemMeta();
            return;
        }
        if ( material == Material.WRITTEN_BOOK )
            this.bookMeta = (BookMeta) this.itemStack.getItemMeta();
    }

    public Item( Material material ) {
        this( material, 1 );
    }

    public void setName( String displayName ) {
        this.itemMeta.setDisplayName( displayName );
    }

    public void setAmount( int amount ) {
        this.itemStack.setAmount( amount );
    }

    public void setMaterial( Material material ) {
        this.itemStack.setType( material );
    }

    public void setUnbreakable( boolean unbreakable ) {
        this.itemMeta.spigot().setUnbreakable( unbreakable );
    }

    public void setSkullOwner( String skullOwner ) {
        this.skullMeta.setOwner( skullOwner );
    }

    public void addLore( String... lore ) {
        String[] loreArray = lore;
        for ( String string : loreArray ) {
            this.lore.add( string );
        }
    }

    public void setLore( String... lore ) {
        this.removeLore();
        String[] loreArray = lore;
        for ( String string : loreArray ) {
            this.lore.add( string );
        }
    }

    public void setLeatherColor( Color color ) {
        if ( !isLeather() )
            return;
        this.armorMeta.setColor( color );
    }

    public void setBookTitle( String title ) {
        if ( this.bookMeta == null )
            return;
        this.bookMeta.setTitle( title );
    }

    public void setBookAuthor( String author ) {
        if ( this.bookMeta == null )
            return;
        this.bookMeta.setAuthor( author );
    }

    public void setBookPages( String... content ) {
        if ( this.bookMeta == null )
            return;
        this.bookMeta.setPages( content );
    }

    public void setBookPage( int page, String content ) {
        if ( this.bookMeta == null )
            return;
        this.bookMeta.setPage( page, content );
    }

    public void addBookPage( String... content ) {
        if ( this.bookMeta == null )
            return;
        this.bookMeta.addPage( content );
    }


    public void removeLore( ) {
        this.lore.clear();
    }

    public void setColor( int colorid ) {
        this.dyecolor = (byte) colorid;
    }

    public void setData( int data ) {
        this.dataid = (byte) data;
    }

    public void setPotion( int potion ) {
        this.potionid = (byte) potion;
    }

    public void addEnchantment( Enchantment enchantment, int level ) {
        this.itemMeta.addEnchant( enchantment, level, true );
    }

    public String getName( ) {
        return this.itemMeta.getDisplayName();
    }

    public int getAmount( ) {
        return this.itemStack.getAmount();
    }

    public Material getMaterial( ) {
        return this.itemStack.getType();
    }

    public boolean isUnbreakable( ) {
        return this.itemMeta.spigot().isUnbreakable();
    }

    public String getSkullOwner( ) {
        return this.skullMeta.getOwner();
    }

    public List<String> getLore( ) {
        return this.lore;
    }

    public byte getColor( ) {
        return this.dyecolor;
    }

    public byte getData( ) {
        return this.dataid;
    }

    public byte getPotion( ) {
        return this.potionid;
    }

    private boolean isLeather( ) {
        return this.getMaterial() == Material.LEATHER_BOOTS || this.getMaterial() == Material.LEATHER_CHESTPLATE ||
                this.getMaterial() == Material.LEATHER_HELMET || this.getMaterial() == Material.LEATHER_LEGGINGS;
    }

    public ItemStack getItem( ) {
        if ( this.dyecolor < 20 )
            this.itemStack = new ItemStack( this.itemStack.getType(), this.itemStack.getAmount(), this.dyecolor );
        if ( this.potionid > 0 )
            this.itemStack = new ItemStack( this.itemStack.getType(), this.itemStack.getAmount(), this.potionid );
        if ( this.dataid != 0 )
            this.itemStack = new ItemStack( this.itemStack.getType(), this.itemStack.getAmount(), this.dataid );

        this.itemMeta.setLore( this.lore );

        if ( this.skullMeta != null ) {
            this.skullMeta.setDisplayName( this.getName() );
            this.skullMeta.spigot().setUnbreakable( this.isUnbreakable() );
            this.skullMeta.setLore( this.lore );
            this.itemStack.setItemMeta( this.skullMeta );
            return this.itemStack;
        }
        if ( this.isLeather() ) {
            this.armorMeta.setDisplayName( this.getName() );
            this.armorMeta.spigot().setUnbreakable( this.isUnbreakable() );
            this.armorMeta.setLore( this.lore );
            this.itemStack.setItemMeta( this.armorMeta );
            return this.itemStack;
        }
        if ( this.bookMeta != null ) {
            this.bookMeta.setDisplayName( this.getName() );
            this.bookMeta.spigot().setUnbreakable( this.isUnbreakable() );
            this.bookMeta.setLore( this.lore );
            this.itemStack.setItemMeta( this.bookMeta );
            return this.itemStack;
        }
        this.itemStack.setItemMeta( this.itemMeta );
        return this.itemStack;
    }
}
