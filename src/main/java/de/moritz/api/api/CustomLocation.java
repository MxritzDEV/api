package de.moritz.api.api;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.configuration.file.FileConfiguration;

import java.io.File;
import java.io.IOException;

/**
 * Created by Moritz on 02.03.2017.
 */

public class CustomLocation {

    private String name;
    private File file;
    private FileConfiguration configuration;
    private Location location;


    public CustomLocation( String name, File file, FileConfiguration configuration ) {
        this.name = name;
        this.file = file;
        this.configuration = configuration;

        if ( exits() )
            this.location = this.getLocation();
    }

    public String getWorld( ) {
        if ( this.location == null )
            return this.configuration.getString( "Location." + this.name + ".World" );
        return this.location.getWorld().getName();
    }

    public double getX( ) {
        if ( this.location == null )
            return this.configuration.getDouble( "Location." + this.name + ".X" );
        return this.location.getX();
    }

    public double getY( ) {
        if ( this.location == null )
            return this.configuration.getDouble( "Location." + this.name + ".Y" );
        return this.location.getY();
    }

    public double getZ( ) {
        if ( this.location == null )
            return this.configuration.getDouble( "Location." + this.name + ".Z" );
        return this.location.getZ();
    }

    public double getYaw( ) {
        if ( this.location == null )
            return this.configuration.getDouble( "Location." + this.name + ".Yaw" );
        return this.location.getYaw();
    }

    public double getPitch( ) {
        if ( this.location == null )
            return this.configuration.getDouble( "Location." + this.name + ".Pitch" );
        return this.location.getPitch();
    }

    public String getName( ) {
        return this.name;
    }

    public Location getLocation( ) {
        if ( this.location == null )
            return new Location( Bukkit.getWorld( this.getWorld() ), this.getX(), this.getY(), this.getZ(),
                    (float) this.getYaw(), (float) this.getPitch() );
        return this.location;
    }

    public void setWorld( String world ) {
        this.configuration.options().copyDefaults( true );
        this.configuration.set( "Location." + this.name + ".World", world );
        save();
    }

    public void setX( double x ) {
        this.configuration.options().copyDefaults( true );
        this.configuration.set( "Location." + this.name + ".X", x );
        save();
    }

    public void setY( double y ) {
        this.configuration.options().copyDefaults( true );
        this.configuration.set( "Location." + this.name + ".Y", y );
        save();
    }

    public void setZ( double z ) {
        this.configuration.options().copyDefaults( true );
        this.configuration.set( "Location." + this.name + ".Z", z );
        save();
    }

    public void setYaw( double yaw ) {
        this.configuration.options().copyDefaults( true );
        this.configuration.set( "Location." + this.name + ".Yaw", yaw );
        save();
    }

    public void setPitch( double pitch ) {
        this.configuration.options().copyDefaults( true );
        this.configuration.set( "Location." + this.name + ".Pitch", pitch );
        save();
    }

    public void setLocation( String world, double x, double y, double z, double yaw, double pitch ) {
        setWorld( world );
        setX( x );
        setY( y );
        setZ( z );
        setYaw( yaw );
        setPitch( pitch );
    }

    public void setLocation( Location location ) {
        setWorld( location.getWorld().getName() );
        setX( location.getX() );
        setY( location.getY() );
        setZ( location.getZ() );
        setYaw( location.getYaw() );
        setPitch( location.getPitch() );
    }

    public boolean exits( ) {
        return this.configuration.contains( "Location." + name );
    }

    public String toString( ) {
        return "World=(" + this.getWorld() + "), X=(" + this.getX() + "), Y=(" + this.getY() + ")," +
                " Z=(" + this.getZ() + "), Yaw=(" + this.getYaw() + "), Pitch=(" + this.getPitch() + ")";
    }

    public void save( ) {
        try {
            this.configuration.save( file );
        } catch ( IOException e ) {
            e.printStackTrace();
        }
    }
}
