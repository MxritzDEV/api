package de.moritz.api.api;

import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.events.PacketContainer;
import com.comphenix.protocol.wrappers.WrappedDataWatcher;
import de.moritz.api.APIPlugin;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.lang.reflect.InvocationTargetException;

/**
 * @author MxritzDEV
 */

public class BossbarAPI {

    private Player player;
    private int entityId;
    private String message;
    private float health;

    public BossbarAPI( Player player, String message, float health ) {
        this.player = player;
        this.entityId = (int) Math.ceil( Math.random() * 1000 ) + 2000;
        this.message = message;
        this.health = health;
    }

    public void setBar( ) {
        PacketContainer spawnPacket = new PacketContainer( PacketType.Play.Server.SPAWN_ENTITY_LIVING );
        Location spawnLocation = player.getEyeLocation().add(
                player.getEyeLocation().getDirection().normalize().multiply( 25 ) );

        spawnPacket.getModifier().write( 0, this.entityId );
        spawnPacket.getModifier().write( 1, (byte) 64 );
        spawnPacket.getModifier().write( 2, spawnLocation.getBlockX() * 32 );
        spawnPacket.getModifier().write( 3, spawnLocation.getBlockY() * 32 );
        spawnPacket.getModifier().write( 4, spawnLocation.getBlockZ() * 32 );

        WrappedDataWatcher watcher = new WrappedDataWatcher();
        watcher.setObject( 0, (byte) 32 );
        watcher.setObject( 2, this.message );
        watcher.setObject( 6, this.health, true );
        watcher.setObject( 10, this.message );
        watcher.setObject( 20, 881 );
        spawnPacket.getDataWatcherModifier().write( 0, watcher );

        try {
            APIPlugin.getProtocolManager().sendServerPacket( this.player, spawnPacket );
            APIPlugin.getBossbarHashMap().put( this.player.getUniqueId(), this );
        } catch ( InvocationTargetException e ) {
            e.printStackTrace();
        }
    }

    public void removeBar( ) {
        PacketContainer despawnPacket = new PacketContainer( PacketType.Play.Server.ENTITY_DESTROY );
        despawnPacket.getIntegerArrays().write( 0, new int[]{ this.entityId } );

        BukkitRunnable runnable = new BukkitRunnable() {
            @Override
            public void run( ) {
                try {
                    APIPlugin.getProtocolManager().sendServerPacket( player, despawnPacket );
                    APIPlugin.getBossbarHashMap().remove( player.getUniqueId(), this );
                } catch ( InvocationTargetException e ) {
                    e.printStackTrace();
                }
            }
        };
        runnable.runTaskLater( APIPlugin.getInstance(), 2 );
    }
}
