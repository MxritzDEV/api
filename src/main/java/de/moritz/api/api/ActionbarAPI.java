package de.moritz.api.api;

import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.events.PacketContainer;
import com.comphenix.protocol.wrappers.WrappedChatComponent;
import de.moritz.api.APIPlugin;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.lang.reflect.InvocationTargetException;

/**
 * Created by Moritz on 02.03.2017.
 */

public class ActionbarAPI {

    public static void sendActionbar( Player player, String message ) {
        PacketContainer packetChat = new PacketContainer( PacketType.Play.Server.CHAT );
        packetChat.getChatComponents().write( 0, WrappedChatComponent.fromJson( "{\"text\": \"" + message + "\"}" ) );
        packetChat.getBytes().write( 0, (byte) 2 );

        try {
            APIPlugin.getProtocolManager().sendServerPacket( player, packetChat );
        } catch ( InvocationTargetException e ) {
            e.printStackTrace();
        }
    }

    public static void sendActionbar( final String message ) {
        Bukkit.getOnlinePlayers().forEach( all -> sendActionbar( all, message ) );
    }
}
